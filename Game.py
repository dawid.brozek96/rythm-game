#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pygame, sys
import numpy as np
from pygame.locals import *
import time
import pandas as pd


# inicjacja modułu pygame
pygame.init()


#music
file = 'm.wav'
pygame.mixer.init()
pygame.mixer.music.load(file)
pygame.mixer.music.play(1) #TODO -1 0 None


ouch = pygame.mixer.Sound("hit.mp3") #hit circle music

FPS = 240

# obiekt zegara, który pozwala śledzić czas
fpsClock = pygame.time.Clock()

# szerokość i wysokość okna gry
OKNOGRY_SZER = 1366
OKNOGRY_WYS = 768

# przygotowanie powierzchni do rysowania, czyli inicjacja okna gry
OKNOGRY = pygame.display.set_mode((OKNOGRY_SZER, OKNOGRY_WYS), 0, 32)


pygame.display.set_caption('Rythm game') # window name

#RGB colors
LT_BLUE = (230, 255, 255)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)




# Inicjacja PALETEK:
# utworzenie powierzchni dla obrazka, wypełnienie jej kolorem,
# pobranie prostokątnego obszaru obrazka i ustawienie go na wstępnej pozycji

PALETKA_1_POZ = (350, 360)  # początkowa pozycja paletki gracza




#pointer
pointer_radius = 15
paletka1_obr = pygame.Surface([pointer_radius, pointer_radius], pygame.SRCALPHA, 32).convert_alpha()
pygame.draw.ellipse(paletka1_obr, BLUE, [0, 0, pointer_radius, pointer_radius])
paletka1_prost = paletka1_obr.get_rect()
paletka1_prost.x = PALETKA_1_POZ[0]
paletka1_prost.y = PALETKA_1_POZ[1]


#``````````````````````````````````````````
# TODO delte in future (save for detect game freezing)
PALETKA_2_POZ = (350, 20)  # początkowa pozycja paletki komputera
paletka2_obr = pygame.Surface([pointer_radius, pointer_radius])
paletka2_obr.fill(RED)
paletka2_prost = paletka2_obr.get_rect()
paletka2_prost.x = PALETKA_2_POZ[0]
paletka2_prost.y = PALETKA_2_POZ[1]
AI_PREDKOSC = 3
#`````````````````````````````````````````````



# Inicjacja PIŁKI
# szerokość, wysokość, prędkość pozioma (x) i pionowa (y) PIŁKI
# utworzenie powierzchni dla piłki, narysowanie na niej koła, ustawienie pozycji początkowej

circle_radius = 50
circle_rot = pygame.Surface([circle_radius, circle_radius], pygame.SRCALPHA, 32).convert_alpha()
circle_co_ordinates = circle_rot.get_rect()
circle_co_ordinates.x = np.random.uniform(low=388, high=978, size=(1,))
circle_co_ordinates.y = np.random.uniform(low=84, high=684, size=(1,))


# Rysowanie komunikatów tekstowych
# ustawienie początkowych wartości liczników punktów
# utworzenie obiektu czcionki z podanego pliku o podanym rozmiarze
GRACZ_1_PKT = '0'
GRACZ_2_PKT = '0'
fontObj = pygame.font.Font('freesansbold.ttf', 64)


# funkcje wyświetlające punkty gracza
# tworzą nowy obrazek z tekstem, pobierają prostokątny obszar obrazka,
# pozycjonują go i rysują w oknie gry

def drukuj_punkty_p1():
    tekst_obr1 = fontObj.render(GRACZ_1_PKT, True, (0, 0, 0))
    tekst_prost1 = tekst_obr1.get_rect()
    tekst_prost1.center = (OKNOGRY_SZER / 2, OKNOGRY_WYS * 0.75)
    OKNOGRY.blit(tekst_obr1, tekst_prost1)


def drukuj_punkty_p2():
    tekst_obr2 = fontObj.render(GRACZ_2_PKT, True, (0, 0, 0))
    tekst_prost2 = tekst_obr2.get_rect()
    tekst_prost2.center = (OKNOGRY_SZER / 2, OKNOGRY_WYS / 4)
    OKNOGRY.blit(tekst_obr2, tekst_prost2)


def nowe_pole_pilka():
    pygame.draw.ellipse(circle_rot, WHITE, [0, 0, circle_radius, circle_radius])
    ax = circle_co_ordinates.x = np.random.uniform(low=333, high=1000, size=(1,)) # new random point
    ay = circle_co_ordinates.y = np.random.uniform(low=50, high=600, size=(1,)) # new random point
    return ax, ay


def reader_peak():
    data_r = pd.read_csv('peak_diff.txt', header=None)
    data_list = data_r.values.tolist()
    data_arr2d = np.array(data_list)
    data_arr = data_arr2d.flatten()
    return data_arr


def now():
    a = time.clock()
    return a

def timer():
    target_time = now() + 0.5
    while True:
        if target_time <= now():
            break

def semi_delte(): #TODO
    pygame.draw.ellipse(circle_rot, BLACK, [0, 0, circle_radius, circle_radius])


# def circle_position():
#     self.itype = item_type
#     self.pos = [x, y]
a = 0
start = time.clock()
t1=0
trafiony= True
i=0
time_diffrent = reader_peak()
# pętla główna programu
while True:
    # future()
    t = time.time()
    if t - t1 >= time_diffrent[i]:
        t1 = time.time()
        print ('tutaj2')
        i+=1
        nowe_pole_pilka()
        if i == len(reader_peak()): #TODO add it and improve
            break


    # obsługa zdarzeń generowanych przez gracza
    for event in pygame.event.get():
        # przechwyć zamknięcie okna
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        # przechwyć ruch myszy
        if event.type == MOUSEMOTION:
            # pobierz współrzędne x, y kursora myszy
            myszaX, myszaY = event.pos

            # przesunięcie paletki gracza
            przesuniecieX = myszaX
            przesuniecieY = myszaY

            # jeżeli wykraczamy poza okno gry w prawo
            if przesuniecieX > OKNOGRY_SZER - pointer_radius:
                przesuniecieX = OKNOGRY_SZER - pointer_radius
            # jeżeli wykraczamy poza okno gry w lewo
            if przesuniecieX < 0:
                przesuniecie = 0
            paletka1_prost.x = przesuniecieX
            paletka1_prost.y = przesuniecieY

        # if event.type == pygame.KEYDOWN:
        #     if event.key == pygame.K_z:
        #         PILKA_PREDKOSC_Y *= -1
        #         # uwzględnij nachodzenie paletki na piłkę (przysłonięcie)
        #         circle_co_ordinates.top = paletka1_prost.bottom

        if event.type == pygame.KEYDOWN:
            if (event.key == pygame.K_z) or (event.key == pygame.K_x) :
                if circle_co_ordinates.colliderect(paletka1_prost):
                    ouch.play()  # sound
                    semi_delte() # TODO



                    # diff = 0
                    # while diff < 500:
                    #     time1 = pygame.time.get_ticks()
                    #     diff = time1 - time2

                    # circle_co_ordinates.x = np.random.uniform(low=333, high=1000, size=(1,))
                    # circle_co_ordinates.y = np.random.uniform(low=50, high=600, size=(1,))

                    # while diff > 0.5:
                    #     time2 = pygame.time.get_ticks()
                    #     diff = time2 - time1

                    # if ( diff > 2):

                    # a = time.clock()
                    #     circle_co_ordinates.x = np.random.uniform(low=333, high=1000, size=(1,))
                    #     circle_co_ordinates.y = np.random.uniform(low=50, high=600, size=(1,))
                    # tick()
                    # start = time.clock()


                    # uwzględnij nachodzenie paletki na piłkę (przysłonięcie)
                    # circle_co_ordinates.top = paletka2_prost.bottom


        # if (myszaX == circle_co_ordinates.x):
        #     PILKA_PREDKOSC_Y *= -1
        #             # uwzględnij nachodzenie paletki na piłkę (przysłonięcie)
        #     circle_co_ordinates.top = paletka1_prost.bottom

    # AI (jak gra komputer)
    # jeżeli środek piłki jest większy niż środek paletki AI
    # przesuń w prawo paletkę z ustawioną prędkością
    if circle_co_ordinates.centerx > paletka2_prost.centerx:
        paletka2_prost.x += AI_PREDKOSC
    # w przeciwnym wypadku przesuń w lewo
    elif circle_co_ordinates.centerx < paletka2_prost.centerx:
        paletka2_prost.x -= AI_PREDKOSC

    # przesuń piłkę po zdarzeniu
    # circle_co_ordinates.x += PILKA_PREDKOSC_X
    # circle_co_ordinates.y += PILKA_PREDKOSC_Y

    # Sprawdzamy kolizje piłki z obiektami
    # Ściany: jeżeli piłka wykracza poza okn0 z lewej lub prawej strony odbij ją od ściany



    # for event in pygame.event.get():
    #
    #     if event.type == pygame.KEYDOWN:
    #         if event.key == pygame.K_z:
    #             # print("Właśnie wciśnięto ESC")
    #             # esc_pressed = True
    #             PILKA_PREDKOSC_Y *= -11
    #             # uwzględnij nachodzenie paletki na piłkę (przysłonięcie)
    #             circle_co_ordinates.top = paletka1_prost.bottom



        # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    # if circle_co_ordinates.colliderect(paletka1_prost):
    #     PILKA_PREDKOSC_Y *= -11
    #     # uwzględnij nachodzenie paletki na piłkę (przysłonięcie)
    #     circle_co_ordinates.top = paletka1_prost.bottom

    if circle_co_ordinates.colliderect(paletka2_prost):

        # uwzględnij nachodzenie paletki na piłkę (przysłonięcie)
        circle_co_ordinates.top = paletka2_prost.bottom

    # jeżeli piłka wyszła poza pole gry u góry lub z dołu ustaw domyślną pozycję piłki
    # i przypisz punkt odpowiedniemu graczowi
    if circle_co_ordinates.top <= 0:
        GRACZ_1_PKT = str(int(GRACZ_1_PKT) + 1)
        # GRACZ_1_PKT = str(int(myszaX))
        # circle_co_ordinates.x = np.random.uniform(low=20, high=200, size=(1,))
        # circle_co_ordinates.y = np.random.uniform(low=20, high=200, size=(1,))

    if circle_co_ordinates.bottom >= OKNOGRY_WYS:
        # circle_co_ordinates.x = np.random.uniform(low=20, high=200, size=(1,))
        # circle_co_ordinates.y = np.random.uniform(low=20, high=200, size=(1,))
        GRACZ_2_PKT = str(int(GRACZ_2_PKT) + 1)

    # circle_co_ordinates
    # GRACZ_1_PKT = str(int(myszaX))
    GRACZ_2_PKT = str(int(circle_co_ordinates.y))
    # Rysowanie obiektów
    OKNOGRY.fill(BLACK)  # kolor okna gry

    drukuj_punkty_p1()  # wyświetl punkty komputera
    drukuj_punkty_p2()  # wyświetl punkty gracza

    # narysuj w oknie gry paletki
    OKNOGRY.blit(paletka1_obr, paletka1_prost)
    OKNOGRY.blit(paletka2_obr, paletka2_prost)

    # narysuj w oknie piłkę
    OKNOGRY.blit(circle_rot, circle_co_ordinates)

    # zaktualizuj okno i wyświetl
    pygame.display.update()

    # zaktualizuj zegar po narysowaniu obiektów
    fpsClock.tick(FPS)


