import matplotlib.pyplot as plt
import numpy as np
import wave, sys
import scipy.signal
import contextlib



def wave_length(fname: str):
    with contextlib.closing(wave.open(fname, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
        # print(duration)
        return duration


def save_timer(peak_list: list, txt_name: str, att: str):
    with open(txt_name, att) as f:
        for item in peak_list:
            f.write("%s\n" % item)


def time_between_next_circle(data_arr2d):
    for i in range(len(data_arr2d)-1):
        out = (data_arr2d[i]- data_arr2d[i+1]) * (-1)
        yield out


def show_peak(sig, peak):
    plt.plot(sig)
    plt.plot(peak, sig[peak], "x")
    plt.show()



def visualize(path: str):
    raw = wave.open(path)

    # reads all the frames
    # -1 indicates all or max frames
    signal = raw.readframes(-1)
    signal = np.frombuffer(signal, dtype="int16")

    # gets the frame rate
    f_rate = raw.getframerate()

    # to Plot the x-axis in seconds
    # you need get the frame rate
    # and divide by size of your signal
    # to create a Time Vector
    # spaced linearly with the size
    # of the audio file
    time = np.linspace(
        0,  # start
        len(signal) / f_rate,
        num=len(signal))

    print('Detect peaks with minimum height and distance filters.')
    # TODO try to find best parameters (in future)
    indexes, _ = scipy.signal.find_peaks(signal, height=20000, distance=48000)
    show_peak(signal, indexes)

    impact_time_factor = wave_length(path) / indexes[len(indexes)-1]
    indexes_value = indexes

    # print(indexes_value)

    for i in range(0, len(indexes)): #converter : which second did the files appear
        indexes[i]=indexes[i] * impact_time_factor


    # print('Peaks are: %s' % (indexes))
    # print("The are: ", len(indexes), " peaks")
    an_array = time
    array_length = len(an_array)
    last_element = an_array[array_length - 1]

    save_timer(indexes, name_txt, "w")

    open(name_txt_diff, 'w').close() # delte content before save a new one
    save_timer(time_between_next_circle(indexes), name_txt_diff, "a")


if __name__ == "__main__":
    path = sys.argv[1]
    name_txt = 'peak.txt'
    name_txt_diff = 'peak_diff.txt'
    visualize(path)
    print("Peaks time saved in {}").format(name_txt)
    print("Difference of peaks saved in {}").format(name_txt_diff)
    # print (type(wave_length(path)))
